//
//  Action.cpp
//  Planning
//
//  Created by Won-Hyung Park on 12. 9. 21..
//  Copyright (c) 2012년 Won-Hyung Park. All rights reserved.
//

#include "Action.h"

Action::Action(ActionType actionType)
{
    this->m_actionType = actionType;
}

void Action::apply(Status *status, int times = 1)
{
    switch (this->m_actionType) {
        case ACTION_TYPE_MAKING_SPUNKS:{
            int spunkers = status->getSpunkers();
            int workbenches = status->getFreeWorkbenches();
            
            if (spunkers >= times && workbenches >= times) {
                status->workBenchWorks(times);
                status->addSpunks(50 * times);
            }
            break;
        }
        case ACTION_TYPE_SERVICING_BLIGS_2:
        {
            int unservicedBligs = status->getUnservicedBligs();
            int bligers = status->getFreeBligers();
            int workbenches = status->getFreeWorkbenches();
            int spunks = status->getSpunks();
            
            if (unservicedBligs >= times && bligers >=  2 * times && workbenches >= times && spunks >= 40 * times) {
                status->bligToQueue(ACTION_TYPE_SERVICING_BLIGS_2, times);
                status->bligerToQueue(ACTION_TYPE_SERVICING_BLIGS_2, times * 2);
                status->workBenchToQueue(ACTION_TYPE_SERVICING_BLIGS_2, times);
                status->spunksUsed(40 * times);
            }
            break;
        }
        case ACTION_TYPE_SERVICING_BLIGS_4:
        {
            int unservicedBligs = status->getUnservicedBligs();
            int bligers = status->getFreeBligers();
            int workbenches = status->getFreeWorkbenches();
            int spunks = status->getSpunks();
            
            if (unservicedBligs >= times && bligers >= times && workbenches >= times && spunks >= 20 * times) {
                status->bligToQueue(ACTION_TYPE_SERVICING_BLIGS_4, times);
                status->bligerToQueue(ACTION_TYPE_SERVICING_BLIGS_4, times);
                status->workBenchToQueue(ACTION_TYPE_SERVICING_BLIGS_4, times);
                status->spunksUsed(20 * times);
            }
            break;
        }
        case ACTION_TYPE_FINDING_PLONKS:
        {
            int plinks = status->getFreePlinks();
            int spunks = status->getSpunks();
            if (plinks >= times && spunks >= 10 * times) {
                status->plinkWorks(times);
                status->spunksUsed(10 * times);
                status->addUnservicedPlonks(10 * times);
            }
            break;
        }
        case ACTION_TYPE_SERVICING_PLONKS:
        {
            int plinks = status->getFreePlinks();
            int servicedBligs = status->getServicedBligs();
            int workbenches = status->getFreeWorkbenches();
            int spunks = status->getSpunks();
            int unservicedPlonks = status->getUnservicedPlonks();
            if (plinks >= times && servicedBligs >= times && workbenches >= times && spunks >= times * 20 && unservicedPlonks >= 5 * times) {
                status->plinkWorks(times);
                status->workBenchWorks(times);
                status->spunksUsed(times * 20);
                status->unservidedPlonksUsed(times * 5);
                
                status->addServicedPlonks(times * 5);
            }
            break;
        }
        case ACTION_TYPE_NOOP:
            break;
    }
}