//
//  Action.h
//  Planning
//
//  Created by Won-Hyung Park on 12. 9. 21..
//  Copyright (c) 2012년 Won-Hyung Park. All rights reserved.
//

#ifndef __Planning__Action__
#define __Planning__Action__

#include <iostream>
#include "Env.h"
#include "Status.h"

class Action {
private:
    ActionType m_actionType;
public:
    Action (ActionType actionType);
    void apply (Status *, int);
};
#endif /* defined(__Planning__Action__) */
