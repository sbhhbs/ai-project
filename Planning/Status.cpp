//
//  Status.cpp
//  Planning
//
//  Created by Won-Hyung Park on 12. 9. 21..
//  Copyright (c) 2012년 Won-Hyung Park. All rights reserved.
//

#include "Status.h"

int Status::getSpunks()
{
    return this->m_spunks;
}

int Status::getUnservicedBligs()
{
    return this->m_unserviced_bligs;
}

int Status::getServicedBligs()
{
    int sum = 0;
    for (int i=0; i<8; i++) {
        sum += this->m_serviced_bligs[i];
    }
    return sum;
}


int Status::getUnservicedPlonks()
{
    return this->m_unserviced_plonks;
}

int Status::getServicedPlonks()
{
    return this->m_serviced_plonks;
}

int Status::getSpunkers()
{
    return this->m_spunkers;
}

int Status::getFreeBligers()
{
    return this->m_free_bligers;
}

int Status::getFreePlinks()
{
    return this->m_free_plinks;
}

int Status::getFreeWorkbenches()
{
    return this->m_free_workbenches;
}

void Status::spunksUsed(int number)
{
    this->m_spunks -= number;
}

void Status::unservidedPlonksUsed(int number)
{
    this->m_unserviced_plonks -= number;
}

void Status::plinkWorks(int number = 1)
{
    this->m_working_plinks += number;
    this->m_free_plinks -= number;
}

void Status::workBenchWorks(int number = 1)
{
    this->m_free_workbenches -= number;
    this->m_working_workbenches += number;
}

void Status::bligToQueue(ActionType actionType, int number = 1)
{
    this->m_unserviced_bligs -= number;
    switch (actionType) {
        case ACTION_TYPE_SERVICING_BLIGS_2:
        {
            this->m_bligs_in_queue[1] += number;
            break;
        }
        case ACTION_TYPE_SERVICING_BLIGS_4:
        {
            this->m_bligs_in_queue[3] += number;
            break;
        }
        default:
            break;
    }
}

void Status::bligerToQueue(ActionType actionType, int number = 1)
{
    switch (actionType) {
        case ACTION_TYPE_SERVICING_BLIGS_2:
        {
            this->m_free_bligers -= number;
            this->m_bligers_in_queue[1] += number;
            break;
        }
        case ACTION_TYPE_SERVICING_BLIGS_4:
        {
            this->m_free_bligers -= number;
            this->m_bligers_in_queue[3] += number;
            break;
        }
        default:
            break;
    }
}

int Status::getCurrentTime()
{
    return m_time;
}

void Status::workBenchToQueue(ActionType actionType, int number = 1)
{
    switch (actionType) {
        case ACTION_TYPE_SERVICING_BLIGS_2:
        {
            this->m_free_workbenches -= number;
            this->m_workbenches_in_queue[1] += number;
            break;
        }
        case ACTION_TYPE_SERVICING_BLIGS_4:
        {
            this->m_free_workbenches -= number;
            this->m_workbenches_in_queue[3] += number;
            break;
        }
        default:
            break;
    }
}

void Status::addSpunks(int number)
{
    this->m_tmp_spunks += number;
}

void Status::addUnservicedPlonks(int number)
{
    // use tmp variable to prevent to be used at same time.
    this->m_tmp_unserviced_plonks += number;
}

void Status::addServicedPlonks(int number)
{
    // use tmp variable to prevent to be used at same time.
    this->m_serviced_plonks += number;
}

bool Status::initialize(int spunkers, int bligers, int plinks, int bligs, int workbenches)
{
    if (spunkers < 1 || bligers < 1 || plinks < 1 || bligs < 1 || workbenches < 1) {
        return false;
    }
    
    if (spunkers + bligers + plinks + bligs + workbenches != 50) {
        return false;
    }
    
    this->m_spunkers = spunkers;
    this->m_free_bligers = bligers;
    this->m_free_plinks = plinks;
    this->m_unserviced_bligs = bligs;
    this->m_free_workbenches = workbenches;
    
    return true;
}

void Status::timeGoes()
{
    this->m_free_plinks += this->m_working_plinks;
    this->m_working_plinks = 0;
    
    this->m_free_workbenches += this->m_working_workbenches;
    this->m_working_workbenches = 0;
    
    this->m_unserviced_bligs += this->m_serviced_bligs[0];
    for (int i=0; i<7; i++) {
        this->m_serviced_bligs[i] = this->m_serviced_bligs[i+1];
    }
    this->m_serviced_bligs[7] = this->m_bligs_in_queue[0];
    this->m_free_bligers += this->m_bligers_in_queue[0];
    this->m_free_workbenches += this->m_workbenches_in_queue[0];
    
    for (int i=0; i<3; i++) {
        this->m_bligs_in_queue[i] = this->m_bligs_in_queue[i+1];
        this->m_bligers_in_queue[i] = this->m_bligers_in_queue[i+1];
        this->m_workbenches_in_queue[i] = this->m_workbenches_in_queue[i+1];
    }

    this->m_bligs_in_queue[3] = 0;
    this->m_bligers_in_queue[3] = 0;
    this->m_workbenches_in_queue[3] = 0;
    
    this->m_spunks += this->m_tmp_spunks;
    this->m_tmp_spunks = 0;
    this->m_unserviced_plonks += this->m_tmp_unserviced_plonks;
    this->m_tmp_unserviced_plonks = 0;
    
    this->m_time += 1;
}

void Status::print()
{
    std::cout << " =========== " << std::endl;
    std::cout << "At " << this->m_time << std::endl;
    std::cout << "spunks : " << this->getSpunks() << std::endl;
    std::cout << "bligs : " << this->getServicedBligs() << std::endl;
    std::cout << "bligs remaining time : [" << this->m_serviced_bligs[0] << "][" << this->m_serviced_bligs[1] << "][" << this->m_serviced_bligs[2] << "][" << this->m_serviced_bligs[3] << "][" << this->m_serviced_bligs[4] << "][" << this->m_serviced_bligs[5] << "][" << this->m_serviced_bligs[6] << "][" << this->m_serviced_bligs[7] << "]" << std::endl;
    
    std::cout << "unservicedPlonks : " << this->getUnservicedPlonks() << std::endl;
    std::cout << "servicedPlonks : " << this->getServicedPlonks() << std::endl << std::endl;;
    
    
    std::cout << "bligs under operation : [" << this->m_bligs_in_queue[0] << "]" << "[" << this->m_bligs_in_queue[1] << "]" << "[" << this->m_bligs_in_queue[2] << "]" << "[" << this->m_bligs_in_queue[3] << "]" << std::endl;
    std::cout << "bligers under operation : [" << this->m_bligers_in_queue[0] << "]" << "[" << this->m_bligers_in_queue[1] << "]" << "[" << this->m_bligers_in_queue[2] << "]" << "[" << this->m_bligers_in_queue[3] << "]" << std::endl;
    std::cout << "workbenches under operation : [" << this->m_workbenches_in_queue[0] << "]" << "[" << this->m_workbenches_in_queue[1] << "]" << "[" << this->m_workbenches_in_queue[2] << "]" << "[" << this->m_workbenches_in_queue[3] << "]" << std::endl;
}