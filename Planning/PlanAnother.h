//
//  PlanAnother.h
//  Planning
//
//  Created by Song on 12-10-4.
//  Copyright (c) 2012年 Won-Hyung Park. All rights reserved.
//

#ifndef Planning_PlanAnother_h
#define Planning_PlanAnother_h

#include<iostream>
#include <vector>
#include <math.h>
using namespace std;


class State
{
public:
	int spunker;
	int workbench;
	int blig;
	int bligger;
	int spunks;
	int plink;
	int unPlonk;
	int sePlonk;
    
    int time;
    int serBligger;
    int bligRemTime;
	State()
	{
		spunker = 0;
		workbench = 0;
		blig = 0;
		spunks = 0;
		unPlonk = 0;
		plink = 0;
		bligger = 0;
		sePlonk = 0;
        time = 0;
        serBligger = 0;
        bligRemTime = 0;
	}
};




void printOut(State&s)
{
    std::cout << " =========== " << std::endl;
    std::cout << "At " << s.time << std::endl;
    std::cout << "spunks : " << s.spunks << std::endl;
    std::cout << "bligs : " << s.serBligger << std::endl;
    /*std::cout << "bligs remaining time : [" << s.bligRemTime == 1 ? s.serBligger : 0 << "][" << s.bligRemTime == 2 ? s.serBligger : 0  << "][" << s.bligRemTime == 3 ? s.serBligger : 0  << "][" << s.bligRemTime == 4 ? s.serBligger : 0  << "][" << s.bligRemTime == 5 ? s.serBligger : 0  << "][" << s.bligRemTime == 6 ? s.serBligger : 0  << "][" << s.bligRemTime == 7 ? s.serBligger : 0  << "][" << s.bligRemTime == 8 ? s.serBligger : 0  << "]" << std::endl;
    */
    std::cout << "unservicedPlonks : " << s.unPlonk << std::endl;
    std::cout << "servicedPlonks : " << s.sePlonk << std::endl << std::endl;;
    
    /*
    std::cout << "bligs under operation : [" << this->m_bligs_in_queue[0] << "]" << "[" << this->m_bligs_in_queue[1] << "]" << "[" << this->m_bligs_in_queue[2] << "]" << "[" << this->m_bligs_in_queue[3] << "]" << std::endl;
    std::cout << "bligers under operation : [" << this->m_bligers_in_queue[0] << "]" << "[" << this->m_bligers_in_queue[1] << "]" << "[" << this->m_bligers_in_queue[2] << "]" << "[" << this->m_bligers_in_queue[3] << "]" << std::endl;
    std::cout << "workbenches under operation : [" << this->m_workbenches_in_queue[0] << "]" << "[" << this->m_workbenches_in_queue[1] << "]" << "[" << this->m_workbenches_in_queue[2] << "]" << "[" << this->m_workbenches_in_queue[3] << "]" << std::endl;
     */

}



int test(State&s,bool print)
{
	int minMoneyFlow = min(s.workbench,s.spunker);
    if(minMoneyFlow == 0)
        return 0;
	
    int minInflow = min(s.plink,min(s.blig,min(s.bligger,s.workbench)));
	if(minInflow == 0)
        return 0;
    
    
    
	while(s.sePlonk < 1000)
	{
        while(s.spunks < 230 * minInflow)
        {
            if(print) printOut(s);
            if(print) cout << "making spunks : " << minMoneyFlow << endl;
            s.spunks += minMoneyFlow * 50;
            s.time ++;
        }
        
        if(print) printOut(s);
		if(print) cout << "finding plonks : " << minInflow << endl;
        if(print) cout << "servicing bligs(4h) : " << minInflow << endl;
        s.unPlonk += 10 * minInflow;
        s.spunks -= 30 * minInflow;
        s.time++;
        
        for(int j = 0; j < 4; j ++)
        {
            if(print) printOut(s);
            if(print) cout << "finding plonks : " << minInflow << endl;
            s.unPlonk += 10 * minInflow;
            s.spunks -= 10 * minInflow;
            s.time++;
        }
                
        s.serBligger = minInflow;
        
        for(int j = 0; j < 8; j ++)
        {
            if(print) printOut(s);
            if(print) cout << "servicing plonks : " << minInflow << endl;
            s.sePlonk += 5 * minInflow;
            s.unPlonk -= 5 * minInflow;
            s.spunks -= 20 * minInflow;
            s.time++;
            if(s.sePlonk >= 1000) goto end;
        }
        
        s.serBligger = 0;

	}
end:
    if(print) printOut(s);
    
back:
	return s.time;
}



#endif
