//
//  main.cpp
//  Planning
//
//  Created by Won-Hyung Park on 12. 9. 21..
//  Copyright (c) 2012년 Won-Hyung Park. All rights reserved.
//

#include <iostream>

#include "Schedular.h"
#include "PlanAnother.h"


int bests[][5] =
{{6 , 10 , 12 , 5 , 17},
 {6 , 10 , 13 , 5 , 16},
 {6 , 11 , 13 , 5 , 15},
 {6 , 10 , 14 , 5 , 15},
 {6 , 11 , 14 , 5 , 14},
 {6 , 10 , 15 , 5 , 14},
 {8 , 10 , 12 , 5 , 15},
 {8 , 10 , 13 , 5 , 14},
 {6 , 10 , 12 , 6 , 16},
 {6 , 11 , 12 , 6 , 15},
 {6 , 10 , 13 , 6 , 15},
 {6 , 10 , 14 , 6 , 14},
 {8 , 10 , 11 , 6 , 15},
 {8 , 10 , 12 , 6 , 14},
 {6 , 10 , 11 , 7 , 16},
 {6 , 12 , 11 , 7 , 14},
 {6 , 10 , 12 , 7 , 15},
 {6 , 10 , 13 , 7 , 14},
 {8 , 10 , 11 , 7 , 14},
 {6 , 10 , 11 , 8 , 15},
 {6 , 11 , 11 , 8 , 14},
 {6 , 10 , 12 , 8 , 14},
 {6 , 10 , 11 , 9 , 14},
 {6 , 10 , 11 , 10 , 13}};
//24




int main(int argc, const char * argv[])
{
    int spunkers = 0;
    int bligers = 0;
    int plinks = 0;
    int bligs = 0;
    int workbenches = 0;
    State s,ss;
    
    
    cout<<"Input Spunker Number: "<<endl;
    cin >> spunkers;
    cout<<"Input Bliger Number: "<<endl;
    cin >> bligers;
    cout<<"Input Plink Number: "<<endl;
    cin >> plinks;
    cout<<"Input Blig Number: "<<endl;
    cin >> bligs;
    cout<<"Input Workbench Number: "<<endl;
    cin >> workbenches;
    
    if(spunkers * bligers * plinks * bligs * workbenches == 0)
    {
        cout<<"Oops. Give me one item each at least!"<<endl;
        return 0;
    }
    if(spunkers + bligers + plinks + bligs + workbenches != 50)
    {
        cout<<"...I Need exactly 50 items..."<<endl;
        return 0;
    }
    s.workbench = workbenches;
    s.blig = bligs;
    s.bligger = bligers;
    s.plink = plinks;
    s.spunker = spunkers;
    
    
    int answer1 = Schedular::execute(spunkers, bligers, plinks, bligs, workbenches,false);
    int answer2 = test(s,false);
    if(answer1 > answer2)
    {
        ss.workbench = workbenches;
        ss.blig = bligs;
        ss.bligger = bligers;
        ss.plink = plinks;
        ss.spunker = spunkers;
        
        test(s,true);
    }
    else
    {
        Schedular::execute(spunkers, bligers, plinks, bligs, workbenches,true);
    }
    
    cout<<endl<<"Suggestion: "<<endl;
    if(answer1 <= 31)
    {
        cout<<"Your resource is already the best!"<<endl;
    }
    else
    {
        int r[24];
        int min,index;
        for(int i = 0; i < 24; i++)
        {
            r[i] = abs(bests[i][0] - bligers) +
                abs(bests[i][1] - bligs) +
                abs(bests[i][2] - plinks) +
                abs(bests[i][3] - spunkers) +
                abs(bests[i][4] - workbenches);
            if(i == 0)
            {
                min = r[i];
                index = 0;
            }
            if(r[i] < min)
            {
                min = r[i];
                index = i;
            }
        }
        cout<<"You could make resource better by making it: "<<endl
            <<"Bliger: "<<bests[index][0]<<endl
            <<"Blig: "<<bests[index][1]<<endl
            <<"Plink: "<<bests[index][2]<<endl
            <<"Spunker: "<<bests[index][3]<<endl
            <<"Workbenches: "<<bests[index][4]<<endl;
    }
    
    
    return 0;
}
/*
int o(int argc, const char * argv[])
{
    int bestspunkers = 0;
    int bestbligers = 0;
    int bestplinks = 0;
    int bestbligs = 0;
    int bestworkbenches = 0;
    State s,ss;
    

    int min = 100000;
    
    
    
    cout<<"[";
    for(int i = 1; i < 50; i++)
	{
		for(int j = 1; j < 50 - i; j++)
		{
			for(int k = 1; k < 50 - j - i; k++)
			{
				for(int l = 1; l < 50 - j - k -i; l++)
				{
					int m = 50 - i - j - k - l;
					int spunkers = i;
                    int bligers = j;
                    int plinks = k;
                    int bligs = l;
                    int workbenches = m;
                    
                    int answer1 = Schedular::execute(spunkers, bligers, plinks, bligs, workbenches,false);
                    if(answer1 == 31)
                    {
                        min = answer1;
                        bestbligers = bligers;
                        bestbligs = bligs;
                        bestspunkers = spunkers;
                        bestworkbenches = workbenches;
                        bestplinks = plinks;
                        
                        cout
                            <<"{"<<bligers
                            <<" , "<<bligs
                            <<" , "<<plinks
                            <<" , "<<spunkers
                            <<" , "<< workbenches<<"},"<<endl;
                    }
                    
				}
			}
            
		}
	}
    
    return 0;
}

*/