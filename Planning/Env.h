//
//  Resource.h
//  Planning
//
//  Created by Won-Hyung Park on 12. 9. 21..
//  Copyright (c) 2012년 Won-Hyung Park. All rights reserved.
//

#ifndef Planning_Env_h
#define Planning_Env_h

enum ActionType {
    ACTION_TYPE_MAKING_SPUNKS,
    ACTION_TYPE_SERVICING_BLIGS_2,
    ACTION_TYPE_SERVICING_BLIGS_4,
    ACTION_TYPE_FINDING_PLONKS,
    ACTION_TYPE_SERVICING_PLONKS,
    ACTION_TYPE_NOOP
};

#endif
