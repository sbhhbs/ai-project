//
//  Status.h
//  Planning
//
//  Created by Won-Hyung Park on 12. 9. 21..
//  Copyright (c) 2012년 Won-Hyung Park. All rights reserved.
//

#ifndef __Planning__Status__
#define __Planning__Status__

#include <iostream>
#include "Env.h"

class Status {
private:
    int m_time;
    
    int m_spunks;
    int m_unserviced_bligs;
    int m_serviced_plonks;
    int m_unserviced_plonks;
    
    int m_spunkers; // all spunkers are free before action applies.
    int m_free_bligers; // working bliger is m_bligers_in_queue
    int m_working_plinks;
    int m_free_plinks;
    
    int m_working_workbenches;
    int m_free_workbenches;
    int m_serviced_bligs[8];
    
    int m_bligs_in_queue[4]; // bligs in operation
    int m_bligers_in_queue[4]; // blingers in operation
    int m_workbenches_in_queue[4]; // workbenches in operation
    
    int m_tmp_spunks; // spunks made at some time(t) can be used at t + 1
    int m_tmp_unserviced_plonks; // unserviced plonks made at some time(t) can be used at t + 1
public:
    int getSpunks();
    int getUnservicedBligs();
    int getServicedBligs();
    int getUnservicedPlonks();
    int getServicedPlonks();
    
    int getSpunkers();
    int getFreeBligers();
    int getFreePlinks();
    int getFreeWorkbenches();
    
    void spunksUsed(int);
    void unservidedPlonksUsed(int);
    void plinkWorks(int);
    void workBenchWorks(int);
    
    // for servicing bligs operation.
    void bligToQueue(ActionType, int);
    void bligerToQueue(ActionType, int);
    void workBenchToQueue(ActionType, int);
    
    // for result of each operations.
    void addSpunks(int);
    void addUnservicedPlonks(int);
    void addServicedPlonks(int);
    
    bool initialize(int, int, int, int, int);
    void timeGoes();
    void print();
    
    int getCurrentTime();
};
#endif /* defined(__Planning__Status__) */
