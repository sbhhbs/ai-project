//
//  Schedular.h
//  Planning
//
//  Created by Won-Hyung Park on 12. 9. 23..
//  Copyright (c) 2012년 Won-Hyung Park. All rights reserved.
//

#ifndef __Planning__Schedular__
#define __Planning__Schedular__

#include <iostream>

#include "Status.h"
#include "Action.h"

class Schedular {
public:
    static int execute(int, int, int, int, int, bool);
};

#endif /* defined(__Planning__Schedular__) */
