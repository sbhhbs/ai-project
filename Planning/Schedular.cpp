//
//  Schedular.cpp
//  Planning
//
//  Created by Won-Hyung Park on 12. 9. 23..
//  Copyright (c) 2012년 Won-Hyung Park. All rights reserved.
//

#include "Schedular.h"

using namespace std;

int Schedular::execute(int spunkers, int bligers, int plinks, int bligs, int workbenches,bool print) {
    Status *status = new Status();
    
    
    if (!status->initialize(spunkers, bligers, plinks, bligs, workbenches)) {
        cout << "degenerate case !" << endl;
        return 0;
    }
    
    
    // Specify algorithm here
    while (status->getServicedPlonks() < 1000) {
        if(print) status->print();
        
        int spunks, unservicedPlonks, unservicedBligs, workingBligs, spunkers, bligers, plinks, workbenches, times;
        int parameters[5];
        Action *action;
        
        // Servicing Plonks
        plinks = status->getFreePlinks();
        workingBligs = status->getServicedBligs();
        workbenches = status->getFreeWorkbenches();
        spunks = status->getSpunks();
        unservicedPlonks = status->getUnservicedPlonks();
        
        parameters[0] = plinks;
        parameters[1] = workingBligs;
        parameters[2] = workbenches;
        parameters[3] = spunks / 20;
        parameters[4] = unservicedPlonks / 5;
        times = *min_element(parameters, parameters  + 5);
        
        action = new Action(ACTION_TYPE_SERVICING_PLONKS);
        action->apply(status, times);
        delete action;
        if(print && times) cout << "servicing plonks : " << times << endl;
        
        
        // Finding Plonks
        unservicedPlonks = status->getUnservicedPlonks();
        plinks = status->getFreePlinks();
        spunks = status->getSpunks();
        if (unservicedPlonks * 4 < spunks) {
            parameters[0] = plinks;
            parameters[1] = spunks / 10;
            times = *min_element(parameters, parameters+2);
            
            action = new Action(ACTION_TYPE_FINDING_PLONKS);
            action->apply(status, times);
            delete action;
            if(print && times) cout << "finding plonks : " << times << endl;
        }
        
        // Servicing Bligs
        workingBligs = status->getServicedBligs();
        unservicedPlonks = status->getUnservicedPlonks();
        
        if (workingBligs < unservicedPlonks / 5){
            unservicedBligs = status->getUnservicedBligs();
            bligers = status->getFreeBligers();
            workbenches = status->getFreeWorkbenches();
            spunks = status->getSpunks();
            
            parameters[0] = unservicedBligs;
            parameters[1] = bligers / 2;
            parameters[2] = workbenches;
            parameters[3] = spunks / 40;
            int t1 = *min_element(parameters, parameters+4);
            
            parameters[0] = unservicedBligs;
            parameters[1] = bligers;
            parameters[2] = workbenches;
            parameters[3] = spunks / 20;
            int t2 = *min_element(parameters, parameters+4);
            if (t2 <= t1 * 2) {
                action = new Action(ACTION_TYPE_SERVICING_BLIGS_2);
                action->apply(status, t1);
                delete action;
                
                if(print && t1) cout << "servicing bligs(2h) : " << t1 << endl;
            } else {
                action = new Action(ACTION_TYPE_SERVICING_BLIGS_4);
                action->apply(status, t2);
                delete action;
                
                if(print && t2) cout << "servicing bligs(4h) : " << t2 << endl;
            }
        }
        
        // Making Spunks
        spunkers = status->getSpunkers();
        workbenches = status->getFreeWorkbenches();
        
        parameters[0] = spunkers;
        parameters[1] = workbenches;
        times = *min_element(parameters, parameters + 2);
        
        action = new Action(ACTION_TYPE_MAKING_SPUNKS);
        action->apply(status, times);
        delete action;
        if(print && times) cout << "making spunks : " << times << endl;
        
        status->timeGoes();
    }
    
    if(print) status->print();
    return status->getCurrentTime();
}